- 👋 Hi, I’m Ngô Minh Nhân
- 👀 I’m interested in Laravel Framework
- 🌱 I’m currently learning Golang
- 💞️ I’m looking to collaborate on ...
- 📫 How to reach me become a man

## 🔧 Technologies & Tools
### OS
![](https://img.shields.io/badge/OS-Windows-informational?style=flat&logo=windows&logoColor=#0078D6)
![](https://img.shields.io/badge/OS-Ubuntu-informational?style=flat&logo=ubuntu&logoColor=#E95420)
![](https://img.shields.io/badge/OS-MacOS-informational?style=flat&logo=macOS&logoColor=#000000)
<br>
### Editor
![](https://img.shields.io/badge/Editor-VSCode_IDEA-informational?style=flat&logo=visual-studio-code&logoColor=#007ACC&color=2bbc8a)
<br>
### Code
![](https://img.shields.io/badge/Code-PHP-critical?style=flat&logo=php&logoColor=#777BB4&color=2bbc8a)
![](https://img.shields.io/badge/Code-JavaScript-critical?style=flat&logo=javascript&logoColor=#F7DF1E&color=2bbc8a)
![](https://img.shields.io/badge/Code-Golang-critical?style=flat&logo=go&logoColor=#00ADD8&color=2bbc8a)
![](https://img.shields.io/badge/Code-Vue-critical?style=flat&logo=vue.js&logoColor=#4FC08D&color=2bbc8a)
<br>
### Tools
![](https://img.shields.io/badge/Tools-MySQL-blueviolet?style=flat&logo=mysql&logoColor=#4479A1&color=2bbc8a)
![](https://img.shields.io/badge/Tools-PostgreSQL-blueviolet?style=flat&logo=postgresql&logoColor=#4169E1&color=2bbc8a)
![](https://img.shields.io/badge/Tools-Neo4j-blueviolet?style=flat&logo=neo4j&logoColor=#008CC1&color=2bbc8a)
![](https://img.shields.io/badge/Tools-Docker-blueviolet?style=flat&logo=docker&logoColor=#2496ED&color=2bbc8a)
![](https://img.shields.io/badge/Tools-Kubernetes-blueviolet?style=flat&logo=kubernetes&logoColor=#326CE5&color=2bbc8a)
<br>

## &#x1f4c8; GitHub Stats

![nK7's GitHub stats](https://github-readme-stats.vercel.app/api?username=nK2708&show_icons=true&theme=tokyonight&count_private=true&include_all_commits=true)



<!-- Resources -->
<!-- Icons: https://simpleicons.org/ -->
<!-- GitHub Stats: https://github.com/anuraghazra/github-readme-stats -->
<!-- Emojis: https://emojipedia.org/emoji/ -->
<!-- HTML Emojis: https://www.fileformat.info/index.htm -->
<!-- Shields: https://shields.io/ -->
<!-- Awesome GitHub Profile README: https://github.com/abhisheknaiidu/awesome-github-profile-readme -->

<!---
nK2708/nK2708 is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
